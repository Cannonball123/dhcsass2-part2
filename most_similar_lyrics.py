# coding=utf-8

# Digital Humanities for CS Assignment 2 part 2

################################################################################################################
################################################### Imports ####################################################
################################################################################################################

import sys
import os
import json
import copy
import math
import operator
import xml.etree.ElementTree as ET

################################################################################################################
################################################### Imports ####################################################
################################################################################################################



################################################################################################################
########################################### Constants and Functions ############################################
################################################################################################################

RES_COUNT = 5

STOP_WORDS = ["אבל", "או", "אולי", "אותה", "אותו", "אותי", "אותך", "אותם", "אותן", "אותנו", "אז", "אחר",
              "אחרות", "אחרי", "אחריכן", "אחרים", "אחרת", "אי", "איזה", "איך", "אין", "איפה", "איתה", "איתו", 
              "איתי", "איתך", "איתכם", "איתכן", "איתם", "איתן", "איתנו", "אך", "אל", "אלה", "אלו", "אם", 
              "אנחנו", "אני", "אס", "אף", "אצל", "אשר", "את", "אתה", "אתכם", "אתכן", "אתם", "אתן", "באמצע", 
              "באמצעות", "בגלל", "בין", "בלי", "במידה", "ברם", "בשביל", "בתוך", "גם", "דרך", "הוא", "היא", 
              "היה", "היכן", "היתה", "היתי", "הם", "הן", "הנה", "הרי", "ואילו", "ואת", "זאת", "זה", "זות", 
              "יהיה", "יוכל", "יוכלו", "יכול", "יכולה", "יכולות", "יכולים", "יכל", "יכלה", "יכלו", "יש", "כאן", 
              "כאשר", "כולם", "כולן", "כזה", "כי", "כיצד", "כך", "ככה", "כל", "כלל", "כמו", "כן", "כפי", "כש", 
              "לא", "לאו", "לאן", "לבין", "לה", "להיות", "להם", "להן", "לו", "לי", "לכם", "לכן", "למה", "למטה", 
              "למעלה", "למרות", "לנו", "לעבר", "לעיכן", "לפיכך", "לפני", "מאד", "מאחורי", "מאין", "מאיפה",
              "מבלי", "מבעד", "מדוע", "מה", "מהיכן", "מול", "מחוץ", "מי", "מכאן", "מכיוון", "מלבד", "מן", 
              "מנין", "מסוגל", "מעט", "מעטים", "מעל", "מצד", "מתחת", "מתי", "נגד", "נגר", "נו", "עד", "עז", 
              "על", "עלי", "עליה", "עליהם", "עליהן", "עליו", "עליך", "עליכם", "עלינו", "עם", "עצמה", "עצמהם", 
              "עצמהן", "עצמו", "עצמי", "עצמם", "עצמן", "עצמנו", "פה", "רק", "שוב", "של", "שלה", "שלהם", "שלהן", 
              "שלו", "שלי", "שלך", "שלכה", "שלכם", "שלכן", "שלנו", "שם", "תהיה", "תחת",
              "i", "me", "my", "myself", "we", "our", "ours", "ourselves", "you", "your", "yours",
              "yourself", "yourselves", "he", "him", "his", "himself", "she", "her", "hers", "herself",
              "it", "its", "itself", "they", "them", "their", "theirs", "themselves", "what", "which",
              "who", "whom", "this", "that", "these", "those", "am", "is", "are", "was", "were", "be",
              "been", "being", "have", "has", "had", "having", "do", "does", "did", "doing", "a", "an",
              "the", "and", "but", "if", "or", "because", "as", "until", "while", "of", "at", "by", "for",
              "with", "about", "against", "between", "into", "through", "during", "before", "after",
              "above", "below", "to", "from", "up", "down", "in", "out", "on", "off", "over", "under",
              "again", "further", "then", "once", "here", "there", "when", "where", "why", "how", "all",
              "any", "both", "each", "few", "more", "most", "other", "some", "such", "no", "nor", "not",
              "only", "own", "same", "so", "than", "too", "very", "s", "t", "can", "will", "just", "don",
              "should", "now",
              "I", "Me", "My", "Myself", "We", "Our", "Ours", "Ourselves", "You", "Your", "Yours",
              "Yourself", "Yourselves", "He", "Him", "His", "Himself", "She", "Her", "Hers", "Herself",
              "It", "Its", "Itself", "They", "Them", "Their", "Theirs", "Themselves", "What", "Which",
              "Who", "Whom", "This", "That", "These", "Those", "Am", "Is", "Are", "Was", "Were", "Be",
              "Been", "Being", "Have", "Has", "Had", "Having", "Do", "Does", "Did", "Doing", "A", "An",
              "The", "And", "But", "If", "Or", "Because", "As", "Until", "While", "Of", "At", "By", "For",
              "With", "About", "Against", "Between", "Into", "Through", "During", "Before", "After",
              "Above", "Below", "To", "From", "Up", "Down", "In", "Out", "On", "Off", "Over", "Under",
              "Again", "Further", "Then", "Once", "Here", "There", "When", "Where", "Why", "How", "All",
              "Any", "Both", "Each", "Few", "More", "Most", "Other", "Some", "Such", "No", "Nor", "Not",
              "Only", "Own", "Same", "So", "Than", "Too", "Very", "S", "T", "Can", "Will", "Just", "Don",
              "Should", "Now"]

# Creates am empty vocabulary of words from the directories.
def create_empty_vocabulary(tagged_dir):
	empty_vocabulary = {}

	# Getting the names of the folders
	folders = os.listdir(tagged_dir)
	# print(folders)

	# for each folder (foldername)
	for folder in folders:
		# Getting the names of the files
		files = os.listdir(tagged_dir + "\\" + folder)
		# For each file
		for file in files:
			# Get the file content
			content = open(tagged_dir + "\\" + folder + "\\" + file, "r")
			# Split it into lines
			lines = content.readlines()
			# For each line
			for line in lines:
				# Split it into words
				words = line.split()
				# Append the third word into the dictionary if line isnt empty
				if len(words) > 3:
					if words[2] not in STOP_WORDS:
						empty_vocabulary[words[2]] = 0
			content.close()
	return empty_vocabulary

# Write JSON object to disk
# Credit: This function is imported from the fightorflight project, by Avraham Natan, 2018
# https://bitbucket.org/Cannonball123/fightorflight/src/dd72dd27a8fa5479f45f8cf84ec1556d9b083bfd/Data%20Retrieval/lol_champs_average_stats.py?at=Data-Retrieval&fileviewer=file-view-default
def write_json_dict_to_disk(json_dict_object, json_file_name):
	# Converting the JSON object to a numerically ordered string by keys
	json_dict_object_as_string = json.dumps({x:json_dict_object[x] for x in json_dict_object.keys()}, sort_keys=True)

	# Writing the augmented JSON to a file
	json_file = open(json_file_name, 'wb')
	json_file.write(json_dict_object_as_string)
	json_file.close()

# Count number of documents in a directory, recursively
def count_docs(xml_dir):
	count = 0

	# Getting the names of the folders
	folders = os.listdir(xml_dir)
	# print(folders)

	# for each folder (foldername)
	for folder in folders:
		# Getting the names of the files
		files = os.listdir(xml_dir + "\\" + folder)
		# For each file
		for file in files:
			# Increment the number of files by 1
			count = count + 1
	return count

# Given the empty vocabulary, tagged songs dir and document count, calculate word frequencies for every word in the vocabulary
def calculate_word_frequencies(empty_vocabulary, tagged_dir, documents_count):
	frequencies = copy.deepcopy(empty_vocabulary)

	# Getting the names of the folders
	folders = os.listdir(tagged_dir)
	# print(folders)

	# for each folder (foldername)
	for folder in folders:
		# Getting the names of the files
		files = os.listdir(tagged_dir + "\\" + folder)
		# For each file
		for file in files:
			# Get the file content
			content = open(tagged_dir + "\\" + folder + "\\" + file, "r")
			# Split it into lines
			lines = content.readlines()
			# For each line
			for line in lines:
				# Split it into words
				words = line.split()
				if len(words) > 3:
					# If the word isnt a stop word, increment its count in the frequencies dict
					if words[2] not in STOP_WORDS:
						frequencies[words[2]] += 1
			content.close()

	# For each word_count in the frequencies dict, calculate its log_2_(document_counts/word_count)
	for word_count in frequencies:
		frequencies[word_count] = math.log(documents_count*1.0/frequencies[word_count], 2)

	return frequencies

# Given empty vocabulary, the query and word frequencies, calculate the query vector.
def calculate_vector(empty_vocabulary, document, word_frequencies):
	query_vector = copy.deepcopy(empty_vocabulary)
	max_word_count = 0

	# Get the document chorus content
	chorus_content = open(document + "_Chorus.txt", "r")
	# Split it into lines
	chorus_lines = chorus_content.readlines()
	# For each line
	for line in chorus_lines:
		# Split it into words
		words = line.split()
		if len(words) > 3:
			# If the word isnt a stop word, increment its count in the query_vector dict
			if words[2] not in STOP_WORDS:
				query_vector[words[2]] += 1
				# Update max word count if needed
				if query_vector[words[2]] > max_word_count:
					max_word_count = query_vector[words[2]]
	chorus_content.close()

	# Get the document lyrics content
	lyrics_content = open(document + "_Lyrics.txt", "r")
	# Split it into lines
	lyrics_lines = lyrics_content.readlines()
	# For each line
	for line in lyrics_lines:
		# Split it into words
		words = line.split()
		if len(words) > 3:
			# If the word isnt a stop word, increment its count in the query_vector dict
			if words[2] not in STOP_WORDS:
				query_vector[words[2]] += 1
				# Update max word count if needed
				if query_vector[words[2]] > max_word_count:
					max_word_count = query_vector[words[2]]
	lyrics_content.close()

	# for each word in the query vector, do final math
	for word in query_vector:
		query_vector[word] = (query_vector[word]*1.0/max_word_count) * word_frequencies[word]

	return query_vector

# Given a vector, calculate its length
def calculate_vector_length(vector):
	length = 0.0
	for component in vector:
		length += math.pow(vector[component], 2)
	length = math.sqrt(length)
	return length

# Calculate vector for every document in the database
def calculate_documents_vectors(empty_vocabulary, tagged_dir, xml_dir, word_frequencies):
	vectors = {}

	# Getting the names of the folders
	folders = os.listdir(tagged_dir)

	# for each folder (foldername)
	for folder in folders:
		# Getting the names of the files
		files = os.listdir(xml_dir + "\\" + folder)
		# For each file
		for file in files:
			vectors[folder + "\\" + file] = calculate_vector(EMPTY_VOCABULARY, tagged_dir + "\\" + folder + "\\" + file[:-4] , WORD_FREQUENCIES)
	return vectors

# Calculate vector lengths for the documents in the database, given their vectors
def calculate_documents_lengths(documents_vectors):
	vector_lengths = {}
	for vector in documents_vectors:
		vector_lengths[vector] = calculate_vector_length(documents_vectors[vector])
	return vector_lengths

# Calculate similarities between query and documents given vectors and lengths
def calculate_similarities(query_vector, query_length, documents_vectors, documents_lengths):
	similarities = {}

	for doc in documents_vectors:
		similarities[doc] = 0.0
		for word in query_vector:
			similarities[doc] += query_vector[word]*documents_vectors[doc][word]
		similarities[doc] = similarities[doc] / (query_length*documents_lengths[doc])

	return similarities

# Get metadata about a song given its path
def get_metadata(path):
	metadata = ""

	tree = ET.parse(path)
	root = tree.getroot()
	metadata = ("Title: " + root[1][0][0][0].text + "\n"
			   	"Singer: " + root[0][0][0][1].text + "\n"
			   	"Writer: " + root[0][0][0][2].text + "\n"
			   	"Composer: " + root[0][0][0][3].text + "\n"
			   	"Album: " + root[0][0][0][4].text + "\n"
			   	"Lyrics: " + root[0][0][2][0].text)
	
	return metadata

# Present the n most similar results
def present_most_similar(n, similarities, xml_dir):
	sim = copy.deepcopy(similarities)
	sorted_sim = sorted(sim.items(), key=operator.itemgetter(1))[::-1]

	for i in range(min(n, len(sorted_sim))):
		print(get_metadata(xml_dir + "\\" + sorted_sim[i][0]) + "\n" + "Similarity ranking: " + str(sorted_sim[i][1]) + "\n")

################################################################################################################
########################################### Constants and Functions ############################################
################################################################################################################




################################################################################################################
################################################# Main Program #################################################
################################################################################################################

################################################# DESCRIPTION ##################################################
################################################################################################################
##																											  ##
##	Following the example in this link: http://www.site.uottawa.ca/~diana/csi4107/cosine_tf_idf_example.pdf	  ##
##	This script will achieve its goal by the following algorithm:											  ##
##																											  ##
##	1. Get and check arguments for validity.																  ##
##	2. Create an empty vocabulary of all the words seen throughout the documents, as a dictionary.			  ##
##	3. Count the number of documents.																		  ##
##	4. For every word in the vocabulary, calculate a frequency as seen in lines 8-13 from the first page	  ##
##	   of the example.																						  ##
##	5. Create words vector for the query and calculate its length, as in rectangle 3 from the first page	  ##
##	   and line 5 from the second page of the example.														  ##
##	6. For every document denoted as di (d1, d2, d3,...)													  ##
##	   6.1 Create words vector and calculate its length in the same fasion seen in step 5.					  ##
##	   6.2 Calculate similarity rating as inner product of (di, query) divided by the product of the lengths  ##
##	       of di and the query.																				  ##
##	   6.3 Save the similarity rating inside similarities dictionary.										  ##
##	7. Presenting the names 5 most similar songs to the query, with their similarity ratings, and metadata.	  ##
##																											  ##
## Note: Commented out writes to json files might not work with hebrew charachters. They are not removed,	  ##
##		 For debug purposes.																				  ##
################################################################################################################

###################################### Getting and checking the arguments ######################################
argList = sys.argv
# argList = ['most_similar_lyrics.py', 'Hey', 'test_all_lyrics_tagged', 'test_Lyrics']

# print ("DEBUG: " + str(argList) + "\n")

if len(argList) == 2:
	if argList[1] == "/?":
		print("Finds songs similar to a given song\n")
		print("Usage:\n  python most_similar_lyrics.py <song_lyrics> <tagged_dir> <xml_dir>\n")
		print("Description:\n  most_similar_lyrics searches a database of lyrics for songs that are")
		print("  similar to the given song.\n")
		print("Parameters:")
		print("  song_lyrics\t\tThe filename (without extension) with lyrics to a given song, in tagged format.")
		print("  tagged_dir\t\tLocation for part-of-speech tagged songs folder.")
		print("  xml_dir\t\tLocation for xml tagged songs folder.\n")
		print("Assumptions:")
		print("  The folders <tagged_dir> and <xml_dir> are assumed to match in content recuresively.")
		print("  If not, unpredicted errors may occur.")
		print("  It is assumed that a properly tagged file for chorus and lyrics of the song is present in")
		print("  the folder of the script. For example: if <song_lyrics> is 'Hey', it is assumed that")
		print("  the files 'Hey_Chorus.txt' and 'Hey_Lyrics.txt' exist, and are properly tagged.")
		exit()
	else:
		print("ERROR: Wrong syntax")
		print("Usage:\n  python most_similar_lyrics.py <song_lyrics> <tagged_dir> <xml_dir>\n")
		print("For more help type: python most_similar_lyrics.py /?")
		exit()
elif len(argList) == 4:
	if not os.path.isdir(argList[2]):
		print("ERROR: cannot find folder " + argList[2])
		print("Make sure to specify a valid folder for part-of-speech tagged songs.\n")
		print("Usage:\n  python most_similar_lyrics.py <song_lyrics> <tagged_dir> <xml_dir>\n")
		print("For more help type: python most_similar_lyrics.py /?")
		exit()
	elif not os.path.isdir(argList[3]):
		print("ERROR: cannot find folder " + argList[3])
		print("Make sure to specify a valid folder for xml tagged songs.\n")
		print("Usage:\n  python most_similar_lyrics.py <song_lyrics> <tagged_dir> <xml_dir>\n")
		print("For more help type: python most_similar_lyrics.py /?")
		exit()
	else:
		pass
else:
	print("ERROR: Wrong syntax")
	print("Usage:\n  python most_similar_lyrics.py <song_lyrics> <tagged_dir> <xml_dir>\n")
	print("For more help type: python most_similar_lyrics.py /?")
	exit()
###################################### Getting and checking the arguments ######################################


############################################ Create empty vocabulary ###########################################
# print ("DEBUG: " + str(argList) + "\n")

# Creating the empty vocabulary
EMPTY_VOCABULARY = create_empty_vocabulary(argList[2])
# print(EMPTY_VOCABULARY)
# write_json_dict_to_disk(EMPTY_VOCABULARY, "empty_vocabulary.json")
############################################ Create empty vocabulary ###########################################


########################################## Counting nubmer of documents ########################################
documents_count = count_docs(argList[3])
# print(documents_count)
########################################## Counting nubmer of documents ########################################


########################################### Calculate word frequencies #########################################
WORD_FREQUENCIES = calculate_word_frequencies(EMPTY_VOCABULARY, argList[2], documents_count)
# print(WORD_FREQUENCIES)
# write_json_dict_to_disk(WORD_FREQUENCIES, "word_frequencies.json")
########################################### Calculate word frequencies #########################################


#################################### Calculate query vector and vector length ##################################
query_vector = calculate_vector(EMPTY_VOCABULARY, argList[1], WORD_FREQUENCIES)
# write_json_dict_to_disk(query_vector, "query_vector.json")
query_length = calculate_vector_length(query_vector)
# print(query_length)
#################################### Calculate query vector and vector length ##################################


################################# Calculate documents vectors and vector lengths ###############################
documents_vectors = calculate_documents_vectors(EMPTY_VOCABULARY, argList[2], argList[3], WORD_FREQUENCIES)
# write_json_dict_to_disk(documents_vectors, "documents_vectors.json")
documents_lengths = calculate_documents_lengths(documents_vectors)
# write_json_dict_to_disk(documents_lengths, "documents_lengths.json")
################################# Calculate documents vectors and vector lengths ###############################


################################## Calculate similarities to query per document ################################
similarities = calculate_similarities(query_vector, query_length, documents_vectors, documents_lengths)
# write_json_dict_to_disk(similarities, "similarities.json")
################################## Calculate similarities to query per document ################################


######################################## Present the n most similar songs ######################################
present_most_similar(RES_COUNT, similarities, argList[3])
######################################## Present the n most similar songs ######################################

################################################################################################################
################################################# Main Program #################################################
################################################################################################################
